/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceService;

import Entity.CommentairePost;
import Entity.Post;
import java.util.List;

/**
 *
 * @author Mohamed Bousselmi
 */
public interface IServiceCommentairePost {
    
     public List<CommentairePost> getAll(Post p);
     public void ajouterComment(CommentairePost c);
     public void supprimerCommentaire(CommentairePost Cp) ;
     public void AimerCommentaire(CommentairePost Cp);
     public void NePlussAimerCommentaire(CommentairePost Cp);
     public boolean verif_jaime(CommentairePost Cp);
     public void ResoudrePost(CommentairePost Cp);
     public boolean verif_resoudre(Post p);
    
}
