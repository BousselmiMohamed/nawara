/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceService;

import Entity.CategoriePost;
import Entity.Post;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author Mohamed Bousselmi
 */
public interface IServicePost {
      public void ajouterPost(Post p);
      public List<Post> getAll();
      public List<Post> getAllFront();
      public List<CategoriePost> getAllCategorie();
      public List<Post> rechercherPost(String x);
      public void AimerPost(Post p);
      public void NePlusAimerPost(Post p);
       public boolean verif_jaime(Post p);
      public void supprimerPost(Post p);
      public void RefuserPost(Post p);
      public void AccepterPost(Post p);
      public CategoriePost findByIdCategorie(int id)throws SQLException;
      public Post findByIdPost(int id)throws SQLException;
     
}
