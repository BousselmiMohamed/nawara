/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Date;

/**
 *
 * @author Mohamed Bousselmi
 */
public class CommentairePost {
    private int id;
    private int postid;
    private Post post;
    private int user_id;
    private Utilisateur user;
    private Date date_Reponse;
    private int nbr_jaime;
    private int nbr_alerte;
    private String description;

    public CommentairePost() {
    }

    public CommentairePost(int id, Post post, Utilisateur user, Date date_Reponse, int nbr_jaime, int nbr_alerte, String description) {
        this.id = id;
        this.post = post;
        this.user = user;
        this.date_Reponse = date_Reponse;
        this.nbr_jaime = nbr_jaime;
        this.nbr_alerte = nbr_alerte;
        this.description = description;
    }

    public CommentairePost(Post post, String description) {
        this.post = post;
        this.description = description;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Utilisateur getUser() {
        return user;
    }

    public void setUser(Utilisateur user) {
        this.user = user;
    }

    public Date getDate_Reponse() {
        return date_Reponse;
    }

    public void setDate_Reponse(Date date_Reponse) {
        this.date_Reponse = date_Reponse;
    }

    public int getNbr_jaime() {
        return nbr_jaime;
    }

    public void setNbr_jaime(int nbr_jaime) {
        this.nbr_jaime = nbr_jaime;
    }

    public int getNbr_alerte() {
        return nbr_alerte;
    }

    public void setNbr_alerte(int nbr_alerte) {
        this.nbr_alerte = nbr_alerte;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}
