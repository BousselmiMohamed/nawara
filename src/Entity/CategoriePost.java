/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author Mohamed Bousselmi
 */
public class CategoriePost {
    private int id;
    private String nomcategoriePost;
    private int NbrPosts;

    public CategoriePost() {
    }

    public CategoriePost(int id, String nomcategoriePost) {
        this.id = id;
        this.nomcategoriePost = nomcategoriePost;
    }
    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomcategoriePost() {
        return nomcategoriePost;
    }

    public void setNomcategoriePost(String nomcategoriePost) {
        this.nomcategoriePost = nomcategoriePost;
    }

    public int getNbrPosts() {
        return NbrPosts;
    }

    public void setNbrPosts(int NbrPosts) {
        this.NbrPosts = NbrPosts;
    }

    @Override
    public String toString() {
        return "" + nomcategoriePost + "";
    }
    
    
    
}
