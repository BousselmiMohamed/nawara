/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Date;

/**
 *
 * @author Mohamed Bousselmi
 */
public class Post {
    
   private int id;
   private int catId;
   private CategoriePost categoriePost;
   private int userId;
   private Utilisateur user;
   private String titre;
   private String description;
   private String photo;
   private Date date_post;
   private Date date_envoi;
   private String etat;
   private int nbr_jaime;
   private int nbr_reponse;
   private int status;

    public Post() {
    }

    public Post(int id, CategoriePost categoriePost, Utilisateur user, String titre, String description, String photo, Date date_post, Date date_envoi, String etat,int nbr_jaime,int status,int nbr_reponse) {
        this.id = id;
        this.categoriePost = categoriePost;
        this.user = user;
        this.titre = titre;
        this.description = description;
        this.photo = photo;
        this.date_post = date_post;
        this.date_envoi = date_envoi;
        this.etat = etat;
        this.nbr_jaime=nbr_jaime;
        this.status=status;
        this.nbr_reponse=nbr_reponse;
    }

    public Post(int catId, String titre, String description, String photo) {
        this.catId = catId;
        
        this.titre = titre;
        this.description = description;
        this.photo = photo;
    }

    public Post(int id) {
        this.id = id;
    }

   
   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public CategoriePost getCategoriePost() {
        return categoriePost;
    }

    public void setCategoriePost(CategoriePost categoriePost) {
        this.categoriePost = categoriePost;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Utilisateur getUser() {
        return user;
    }

    public void setUser(Utilisateur user) {
        this.user = user;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDate_post() {
        return date_post;
    }

    public void setDate_post(Date date_post) {
        this.date_post = date_post;
    }

    public Date getDate_envoi() {
        return date_envoi;
    }

    public void setDate_envoi(Date date_envoi) {
        this.date_envoi = date_envoi;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public int getNbr_jaime() {
        return nbr_jaime;
    }

    public void setNbr_jaime(int nbr_jaime) {
        this.nbr_jaime = nbr_jaime;
    }

    public int getNbr_reponse() {
        return nbr_reponse;
    }

    public void setNbr_reponse(int nbr_reponse) {
        this.nbr_reponse = nbr_reponse;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    

    @Override
    public String toString() {
        return "Post{" + "id=" + id + ", catId=" + catId + ", categoriePost=" + categoriePost + ", userId=" + userId + ", user=" + user + ", titre=" + titre + ", description=" + description + ", photo=" + photo + ", date_post=" + date_post + ", date_envoi=" + date_envoi + ", etat=" + etat + ", nbr_jaime=" + nbr_jaime + ", nbr_reponse=" + nbr_reponse + '}';
    }
   
   
   
   
   
   
    
}
