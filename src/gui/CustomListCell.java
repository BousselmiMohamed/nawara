/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity.Post;
import Service.ServiceCommentairePost;

import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Mohamed Bousselmi
 */
 public class CustomListCell extends ListCell<Post> {
        private HBox content;
        private Text user;
        private Text titre;
        private Text date;
        private Text nbr_jaime;
        private Text nbr_comment;
        private Text resolu;
        Button btn = new Button();
       
        private ImageView img;
        

        public CustomListCell() {
            super();
            user = new Text();
            titre = new Text();
            img = new ImageView();
            nbr_jaime = new Text();
            resolu = new Text("Topic Résolu");
            resolu.setFill(Color.GREEN);
            img.setFitHeight(100);
            img.setFitWidth(130);
            
            titre.setFont(Font.font(16));
            titre.setFont(Font.font("System", FontWeight.BOLD, 16));
            date= new Text();
            VBox vBox = new VBox(titre,user,date,resolu);
            vBox.setSpacing(10);
            
            content = new HBox(img, vBox);
            content.setSpacing(50);
        }

        @Override
        protected void updateItem(Post item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null && !empty) { // <== test for null item and empty parameter
                user.setText("Ajouter par : "+item.getUser().toString());
                titre.setText(item.getTitre());
                date.setText("le "+item.getDate_envoi().toString()+"                                                                                                              "+new ServiceCommentairePost().getnbrComment(item)+" commentaires "+item.getNbr_jaime()+" J'aimes");
               // nbr_jaime.setText(item.getNbr_jaime()+" J'aimes");
                javafx.scene.image.Image im = new javafx.scene.image.Image("http://localhost//dotcom_2019//web//img//"+item.getPhoto());   
                img.setImage(im);
                if(item.getStatus()==1)
                {
                    resolu.setVisible(true);
                
                }
                else{ resolu.setVisible(false);}
                setGraphic(content);
            } else {
                setGraphic(null);
            }
        }
    }
    

