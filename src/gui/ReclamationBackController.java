/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity.Reclamation;
import Entity.Session;
import Service.ServiceProduit;
import Service.ServiceReclamation;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage;

/**
 * FXML Controller class
 *
 * @author Mohamed Bousselmi
 */
public class ReclamationBackController implements Initializable {

    @FXML
    private Button btnOverview;
    @FXML
    private Button btnMenus;
    @FXML
    private Button btnCustomers;
    @FXML
    private Button btnPackages;
    @FXML
    private Button btnOrders;
    @FXML
    private Button btnSettings;
    @FXML
    private Button btnSignout;
    @FXML
    private Pane pnlOverview;
    @FXML
    private TabPane tab;
    @FXML
    private TableView<Reclamation> reclamation;
    @FXML
    private TableColumn<Reclamation,String> Utilisateur;
    @FXML
    private TableColumn<Reclamation, String> sujet;
    @FXML
    private TableColumn<Reclamation, String> date_reclamation;
    @FXML
    private TableColumn<Reclamation, String> nom_produit;
    @FXML
    private TableColumn<Reclamation, String> etat_reclamation;
    @FXML
    private Button Consulter;
    @FXML
    private TextField rechercher;
    @FXML
    private ImageView rech;
    @FXML
    private Button valider;
    @FXML
    private Button Annuler_formulaire;
    @FXML
    private JFXTextArea descript;
    @FXML
    private JFXTextField titre;
    @FXML
    private JFXTextField NomProduit;
    @FXML
    private JFXTextField date_Reclamation;
    @FXML
    private JFXTextField user;
    @FXML
    private Button Envoyer_mail;
    @FXML
    private Button Annuler_formulaire1;
    @FXML
    private JFXTextArea Subject;
    @FXML
    private JFXTextField user_mail;
    @FXML
    private JFXTextField Objet;

    @FXML
    private BarChart <String, Integer> z;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis y;
    /**********************My Instance****************/
     private ObservableList<Reclamation> ListReclamation = FXCollections.observableArrayList();
     Reclamation r = new Reclamation(); 
     ServiceReclamation Rs = new ServiceReclamation();
     ServiceProduit Ps= new ServiceProduit();
    Stage dialogStage = new Stage();
    Scene scene;
    /***********************************************/

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Consulter.setVisible(false);
         titre.setEditable(false);
         descript.setEditable(false);
         NomProduit.setEditable(false);
         date_Reclamation.setEditable(false);
         user.setEditable(false);
         user_mail.setEditable(false);
        Afficher();
        
        rechercher.textProperty().addListener(new ChangeListener<String>() {
    @Override
    public void changed(ObservableValue<? extends String> observable,
            String oldValue, String newValue) {
        Consulter.setVisible(false);
       recherche();
    }
    } );
    
    }    
    
     public void reponseR(String m,String d,String o)
    {
        
         try{
            String host ="smtp.gmail.com" ;
           String user = "nawara.dotcom@gmail.com";
            String pass = "nawara1234";
            String to 
                     = m;
            String from = "nawara.dotcom@gmail.com";
            String subject = o;
            String messageText = d;
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject); msg.setSentDate(new java.util.Date());
            msg.setText(messageText);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           System.out.println("message send successfully");
        }catch(Exception ex)
        {
            System.out.println(ex);
        }

    
        
    }
     
     void LoadBarChartData(){
            XYChart.Series set1= new XYChart.Series<>();    
       ObservableList<String> monthNames = FXCollections.observableArrayList();
       String[] months = DateFormatSymbols.getInstance(Locale.FRANCE).getMonths();
       set1.setName("Nombre de post par mois");
       z.getData().clear();
        // Convert it to a list and add it to our ObservableList of months.
        monthNames.addAll(Arrays.asList(months));
        
        // Assign the month names as categories for the horizontal axis.
        x.setCategories(monthNames);
          int[] monthValue =Rs.Stat();
          
        for (int i = 1; i <12; i++) {
            set1.getData().add(new XYChart.Data<>(monthNames.get(i-1), monthValue[i]));
        }
        
         z.getData().addAll(set1);
       
       } 

        void recherche ()
{
 if(rechercher.getText().equals(""))
     {
         Afficher();
     }
     else {
      ListReclamation = FXCollections.observableArrayList(Rs.rechercherReclamation(rechercher.getText()));
       sujet.setCellValueFactory(new PropertyValueFactory<>("sujet"));
        sujet.cellFactoryProperty();
        Utilisateur.setCellValueFactory(new PropertyValueFactory<>("description"));
        Utilisateur.cellFactoryProperty();
        date_reclamation.setCellValueFactory(new PropertyValueFactory<>("dateReclamation"));
        date_reclamation.cellFactoryProperty();
        nom_produit.setCellValueFactory(new PropertyValueFactory<>("produit"));
        nom_produit.cellFactoryProperty();
        
        etat_reclamation.setCellValueFactory(new PropertyValueFactory<>("etat"));
        etat_reclamation.cellFactoryProperty();
         
           reclamation.setItems(ListReclamation);
     }
}
      void Afficher()
    {
        ListReclamation = FXCollections.observableArrayList(Rs.getAll());
        sujet.setCellValueFactory(new PropertyValueFactory<>("sujet"));
        sujet.cellFactoryProperty();
        Utilisateur.setCellValueFactory(new PropertyValueFactory<>("user"));
        Utilisateur.cellFactoryProperty();
        date_reclamation.setCellValueFactory(new PropertyValueFactory<>("dateReclamation"));
        date_reclamation.cellFactoryProperty();
        nom_produit.setCellValueFactory(new PropertyValueFactory<>("produit"));
        nom_produit.cellFactoryProperty();
        
        etat_reclamation.setCellValueFactory(new PropertyValueFactory<>("etat"));
        etat_reclamation.cellFactoryProperty();
         
           reclamation.setItems(ListReclamation);
    }
    
    @FXML
    private void table_clicked(MouseEvent event) {
        if(reclamation.getSelectionModel().getSelectedItem().getEtat().equals("Répondre"))
        {
     
      Consulter.setVisible(true);
            
 }else 
     {
      Consulter.setVisible(false);
     }
    }

    @FXML
    private void afficher_reclamation(ActionEvent event) {
        titre.setText(reclamation.getSelectionModel().getSelectedItem().getSujet());
        descript.setText(reclamation.getSelectionModel().getSelectedItem().getDescription());
        NomProduit.setText(reclamation.getSelectionModel().getSelectedItem().getProduit().toString());
        date_Reclamation.setText(reclamation.getSelectionModel().getSelectedItem().getDateReclamation().toString());
        user.setText(reclamation.getSelectionModel().getSelectedItem().getUser().getNom()+"  "+ reclamation.getSelectionModel().getSelectedItem().getUser().getPrenom());
        tab.getSelectionModel().select(1);
    }

    @FXML
    private void Repondre(ActionEvent event) {
        user_mail.setText(reclamation.getSelectionModel().getSelectedItem().getUser().getEmail());
        Subject.setText("cher(e) madame/monsieur "+reclamation.getSelectionModel().getSelectedItem().getUser().getNom()+"  "+ reclamation.getSelectionModel().getSelectedItem().getUser().getPrenom()+
        "\n\nBonjour,\nNous avons étudié votre réclamation du "+reclamation.getSelectionModel().getSelectedItem().getDateReclamation().toString()+" concernant le produit "+reclamation.getSelectionModel().getSelectedItem().getProduit().toString()+
       "\n\n\nMerci pour votre fidélité(e).\ncordialement\nl'équipe de Nawara"         
        );
        tab.getSelectionModel().select(2);
         
    }

    @FXML
    private void annuler_formulaire(ActionEvent event) {
         tab.getSelectionModel().select(0);
    }

    @FXML
    private void Envoyer(ActionEvent event) {
        
        // reponseR(user_mail.getText(),Subject.getText(),Objet.getText());
         Rs.modifierEtatReclamation(reclamation.getSelectionModel().getSelectedItem());
         Afficher();
         Consulter.setVisible(false);
         tab.getSelectionModel().select(0);
         
         
    }
    
    
     @FXML
    private void Post_btn(ActionEvent event)
     {
    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Post.fxml")));
        } catch (IOException ex) {
            Logger.getLogger(PostFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
                    dialogStage.setScene(scene);
                    dialogStage.show();
        }
    @FXML
    private void logout_btn(ActionEvent event)
     {
    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Login.fxml")));
        } catch (IOException ex) {
            Logger.getLogger(PostFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            Session.close();
         } catch (Exception ex) {
             Logger.getLogger(ReclamationController.class.getName()).log(Level.SEVERE, null, ex);
         }
                    dialogStage.setScene(scene);
                    dialogStage.show();
        }
    
    @FXML
    private void statistique(MouseEvent event) {
         tab.getSelectionModel().select(3);
         
         LoadBarChartData();
         
    
    }
    @FXML
    private void Retour_stat(MouseEvent event) {
      
          tab.getSelectionModel().select(0);
    }
}
