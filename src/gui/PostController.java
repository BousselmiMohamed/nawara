/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;
import Entity.Sms;
import Entity.CategoriePost;
import Entity.Post;
import Entity.Reclamation;
import Entity.Session;
import Entity.Utilisateur;
import Service.ServicePost;
import Service.ServiceProduit;
import Service.ServiceReclamation;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import Service.ServiceSms;
import static Service.ServiceSms.ACCOUNT_SID;
import static Service.ServiceSms.AUTH_TOKEN;
import java.util.ArrayList;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Mohamed Bousselmi
 */
public class PostController implements Initializable {

    @FXML
    private Button btnOverview;
    @FXML
    private Button btnMenus;
    @FXML
    private Button btnCustomers;
    @FXML
    private Button btnPackages;
    @FXML
    private Button btnOrders;
    @FXML
    private Button btnSettings;
    @FXML
    private Button btnSignout;
    @FXML
    private Pane pnlOverview;
    @FXML
    private TabPane tab;
    @FXML
    private TableView<Post> post;
    @FXML
    private TableColumn<Post, String> nbr_comment;
    @FXML
    private TableColumn<Post, String> nbr_like;
    @FXML
    private TableColumn<Post, String> titre;
    @FXML
    private TableColumn<Post, String> Utilisateur;
    @FXML
    private TableColumn<Post, String> Categorie;
    @FXML
    private TableColumn<Post, Date> date_envoi_post;
    @FXML
    private TableColumn<Post, String> Etat_post;
    @FXML
    private Button Supprimer;
    @FXML
    private Button Annuler;
    @FXML
    private Button Consulter;
    @FXML
    private TextField rechercher;
    @FXML
    private ImageView rech;
    @FXML
    private Button Refuser;
    @FXML
    private Button Accepter;
    
    @FXML
    private JFXTextArea descript;
    @FXML
    private JFXTextField user;
    @FXML
    private JFXTextField date_envoi;
    
    @FXML
    private JFXTextField titre_post;
    @FXML
    private ImageView image_Post;
    
    @FXML
    private PieChart Stat;
    
    
    
    @FXML
    private BarChart <String, Integer> z;

    @FXML
    private CategoryAxis x;

    @FXML
    private NumberAxis y;
    
    @FXML
    private Label Stat1;

    @FXML
    private Label Stat2;
    
   /**********************My Instance****************/ 
    javafx.scene.image.Image image =new javafx.scene.image.Image("http://localhost//dotcom_2019//web//img//");
    Stage dialogStage = new Stage();
    Scene scene;
    /***********************************************/
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
         titre_post.setEditable(false);
        date_envoi.setEditable(false);
        user.setEditable(false);
        descript.setEditable(false);

        Afficher();
        rechercher.textProperty().addListener(new ChangeListener<String>() {
    @Override
    public void changed(ObservableValue<? extends String> observable,
            String oldValue, String newValue) {

       recherche();
    }
    } );
        
        Consulter.setVisible(false);
        Supprimer.setVisible(false);
    }    
private ObservableList<Post> ListPost = FXCollections.observableArrayList();
    Post p = new Post(); 
     ServicePost Ps = new ServicePost();
     
     
      
     
         void LoadBarChartData(){
            XYChart.Series set1= new XYChart.Series<>();    
       ObservableList<String> monthNames = FXCollections.observableArrayList();
       String[] months = DateFormatSymbols.getInstance(Locale.FRANCE).getMonths();
       set1.setName("Nombre de post par mois");
       z.getData().clear();
        // Convert it to a list and add it to our ObservableList of months.
        monthNames.addAll(Arrays.asList(months));
       
        // Assign the month names as categories for the horizontal axis.
        x.setCategories(monthNames);
          int[] monthValue =Ps.Stat3();
          
        for (int i = 1; i <12; i++) {
            set1.getData().add(new XYChart.Data<>(monthNames.get(i-1), monthValue[i]));
        }
        
         z.getData().addAll(set1);
       
       } 
    void LoadPieChartData(){
        ObservableList<String> Statistique = Ps.Stat();
        ArrayList a=new ArrayList<String>(Statistique);
           ObservableList<PieChart.Data>pieChartData=FXCollections.observableArrayList ( );
           
           for(int i=0;i<Statistique.size();i+=2)
                       {
                           System.out.println(Statistique.get(i));
                           pieChartData.add(new PieChart.Data(Statistique.get(i+1)+" "+String.valueOf(((Integer.parseInt(Statistique.get(i))*100))/( Ps.getAll().size()))+"%",Integer.parseInt(Statistique.get(i))));
                       }
        
        Stat.setData(pieChartData);
       } 
      void recherche ()
{
 if(rechercher.getText().equals(""))
     {
         Afficher();
     }
     else {
      ListPost = FXCollections.observableArrayList(Ps.rechercherPost(rechercher.getText()));
        
        nbr_comment.setCellValueFactory(new PropertyValueFactory<>("nbr_reponse"));
        nbr_comment.cellFactoryProperty();
        nbr_like.setCellValueFactory(new PropertyValueFactory<>("nbr_jaime"));
        nbr_like.cellFactoryProperty();
        titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        titre.cellFactoryProperty();
        date_envoi_post.setCellValueFactory(new PropertyValueFactory<>("date_envoi"));
        date_envoi_post.cellFactoryProperty();
        Etat_post.setCellValueFactory(new PropertyValueFactory<>("etat"));
        Etat_post.cellFactoryProperty();
        
        
         
           post.setItems(ListPost);
     }
    }
    void Afficher()
    {
        ListPost = FXCollections.observableArrayList(Ps.getAll());
       
        nbr_comment.setCellValueFactory(new PropertyValueFactory<>("nbr_reponse"));
        nbr_comment.cellFactoryProperty();
        nbr_like.setCellValueFactory(new PropertyValueFactory<>("nbr_jaime"));
        nbr_like.cellFactoryProperty();
        titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        titre.cellFactoryProperty();
       
        Utilisateur.setCellValueFactory(new PropertyValueFactory<>("user"));
        Utilisateur.cellFactoryProperty();
        
        Categorie.setCellValueFactory(new PropertyValueFactory<>("categoriePost"));
        Categorie.cellFactoryProperty();
        
        date_envoi_post.setCellValueFactory(new PropertyValueFactory<>("date_envoi"));
        date_envoi_post.cellFactoryProperty();
        Etat_post.setCellValueFactory(new PropertyValueFactory<>("etat"));
        Etat_post.cellFactoryProperty();

           post.setItems(ListPost);
    }
 
     
     
     
     
     
     
    @FXML
    private void table_clicked(MouseEvent event) {
        if(post.getSelectionModel().getSelectedItem().getEtat().equals("En Attente"))
        { Consulter.setVisible(true);
        Supprimer.setLayoutX(420);
        }
        else { Consulter.setVisible(false);
            Supprimer.setLayoutX(310);
                }
        Supprimer.setVisible(true);
    }

    @FXML
    private void afficher_formulaire_post(ActionEvent event) {
        
        titre_post.setText(post.getSelectionModel().getSelectedItem().getTitre());
        date_envoi.setText(post.getSelectionModel().getSelectedItem().getDate_envoi().toString());
        user.setText(post.getSelectionModel().getSelectedItem().getUser().getNom()+" "+post.getSelectionModel().getSelectedItem().getUser().getPrenom());
        descript.setText(post.getSelectionModel().getSelectedItem().getDescription());
        image_Post.setImage(new javafx.scene.image.Image("http://localhost//dotcom_2019//web//img//"+post.getSelectionModel().getSelectedItem().getPhoto()));
        tab.getSelectionModel().select(1);
        Consulter.setVisible(false);
        Supprimer.setVisible(false);
       
    }

    @FXML
    private void Refuser_post(ActionEvent event) {
        ServicePost Ps = new ServicePost();
              Post P = new Post(post.getSelectionModel().getSelectedItem().getId());
               //new ServiceSms().sendSms(new Sms("+216"+post.getSelectionModel().getSelectedItem().getUser().getNumTel(),"cher(e) madame/monsieur "+post.getSelectionModel().getSelectedItem().getUser().getNom()+" "+post.getSelectionModel().getSelectedItem().getUser().getPrenom()+"\nBonjour,\n,votre topic n'est pas malheureusement accepté par notre équipe Nawara"));
        
               Ps.RefuserPost(P);
               Afficher();
               tab.getSelectionModel().select(0);
    }

    @FXML
    private void Accepter_post(ActionEvent event) {
      ServicePost Ps = new ServicePost();
              Post P = new Post(post.getSelectionModel().getSelectedItem().getId());
              // new ServiceSms().sendSms(new Sms("+216"+post.getSelectionModel().getSelectedItem().getUser().getNumTel(),"cher(e) madame/monsieur "+post.getSelectionModel().getSelectedItem().getUser().getNom()+" "+post.getSelectionModel().getSelectedItem().getUser().getPrenom()+"\nBonjour,\nvotre topic a été accepté par notre équipe Nawara\nil est maintenant publiée sur nos platformes"));
        
               Ps.AccepterPost(P);
               Afficher();
               tab.getSelectionModel().select(0);
    }
     @FXML
    private void Annuler_post(ActionEvent event) {
        Consulter.setVisible(false);
        Supprimer.setVisible(false);
        tab.getSelectionModel().select(0);
    }
    
    
     @FXML
    private void supprimer_Post(ActionEvent event) {
        if (!post.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("suppression d'une catégorie d'event");
            alert.setHeaderText("Etes-vous sur de vouloir  supprimer   "
                    + post.getSelectionModel().getSelectedItem().getTitre()+ "?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
               ServicePost Ps = new ServicePost();
               Post p=(Post)post.getSelectionModel().getSelectedItem();
               Ps.supprimerPost(p);
              
                Afficher();
                
                
            }
        }
        Consulter.setVisible(false);
        Supprimer.setVisible(false);
        
    }
    
    @FXML
    private void reclamation_btn(ActionEvent event)
     {
    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/ReclamationBack.fxml")));
        } catch (IOException ex) {
            Logger.getLogger(PostFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
                    dialogStage.setScene(scene);
                    dialogStage.show();
        }
    @FXML
    private void logout_btn(ActionEvent event)
     {
    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Login.fxml")));
        } catch (IOException ex) {
            Logger.getLogger(PostFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Session.close();
         } catch (Exception ex) {
             Logger.getLogger(ReclamationController.class.getName()).log(Level.SEVERE, null, ex);
         }
                    dialogStage.setScene(scene);
                    dialogStage.show();
        }
    
    
    @FXML
    private void statistique(MouseEvent event) {
         tab.getSelectionModel().select(2);
         Stat.setVisible(true);
         LoadPieChartData();
         z.setVisible(false);
         Stat1.setVisible(true);
         Stat2.setVisible(false);
       
         
    }
    @FXML
    private void Retour_stat(MouseEvent event) {
      
          tab.getSelectionModel().select(0);
    }
    
        @FXML
    void statistique1(MouseEvent event) {
        Stat.setVisible(false);
        LoadBarChartData();
        z.setVisible(true);
        
         Stat1.setVisible(false);
         Stat2.setVisible(true);
    }

    @FXML
    void statistique2(MouseEvent event) {
         LoadPieChartData();
        Stat.setVisible(true);
        z.setVisible(false);
         Stat1.setVisible(true);
         Stat2.setVisible(false);
    }
    
}
