/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity.CommentairePost;
import Entity.Post;
import Entity.Session;
import Service.ServiceCommentairePost;
import Service.ServicePost;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Mohamed Bousselmi
 */
public class CommentListCell extends ListCell<CommentairePost> {
    
    private HBox content;
        private Text user;
        
        private Text commentaire;
        private Text date;
        private Text nbr_jaime;
       
        Button supprimer = new Button();
        Button like =new Button();
        Button resolu =new Button("resolu");
       
        private ImageView img;
        private Circle c;

        public CommentListCell() {
            super();
            user = new Text();
            date = new Text();        
            commentaire = new Text();
             img = new ImageView();
            nbr_jaime = new Text();
           // btn.setText("Supprimer");
         //btn.setStyle("-fx-background-color: #f65666;-fx-text-fill : #e7e5e5;");
          img.setFitHeight(33);
            img.setFitWidth(23);
            
         supprimer.setStyle("-fx-background-color: rgba(255, 255, 255,0);");
         like.setStyle("-fx-background-color: rgba(255, 255, 255,0);");
         resolu.setStyle("-fx-background-color: #f65666;-fx-text-fill : #e7e5e5;");
         Image image = new Image("http://localhost//dotcom_2019//web//img//supprimer.png",30,30,false,false);
         supprimer.setGraphic(new ImageView(image));
         image = new Image("http://localhost//dotcom_2019//web//img//jaime.png",33,23,false,false);
         like.setGraphic(new ImageView(image));
          image = new Image("http://localhost//dotcom_2019//web//img//jaimepas.png",33,23,false,false);
        // dislike.setGraphic(new ImageView(image));
             c=new Circle();
           
         c.setRadius(40);
            user.setFont(Font.font("System", FontWeight.BOLD, 16));
            date.setFont(Font.font("System", FontWeight.BOLD, 12));
            HBox hBox = new HBox(user,date);
            hBox.setSpacing(350);
            HBox hbox=new HBox(supprimer,resolu,nbr_jaime,like,img);
             hbox.setSpacing(10);
            VBox vBox = new VBox(hBox,commentaire,hbox);
            vBox.setSpacing(10);
            
            content = new HBox(c, vBox);
            content.setSpacing(50);
         
        }

        @Override
        protected void updateItem(CommentairePost item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null && !empty) { // <== test for null item and empty parameter
                user.setText(item.getUser().toString());
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy ");
       
                date.setText("Le "+dateFormat.format(item.getDate_Reponse()).toString());
                commentaire.setText(item.getDescription());
                     nbr_jaime.setText("                                                                                                 "+item.getNbr_jaime()+" J'aimes");
                //nbr_jaime.setText(item.getNbr_jaime()+" J'aimes");
                img.setVisible(false);
              if(item.getNbr_alerte()==1)
              {javafx.scene.image.Image im = new javafx.scene.image.Image("http://localhost//dotcom_2019//web//img//check.png");   
             img.setImage(im);
              img.setVisible(true);}
          Image j;
          if(!item.getUser().getPhoto().equals(""))
          {  j=new Image("http://localhost//dotcom_2019//web//img//"+item.getUser().getPhoto(),false);
          
          }
          else {  j=new Image("http://localhost//dotcom_2019//web//img//user.png",false);}
                c.setStroke(Color.SEAGREEN);
                c.setFill(new ImagePattern(j));
                try {
                    if(item.getUser().getId()==Session.getCurrentSession())
                    {supprimer.setVisible(true);}
                    else {supprimer.setVisible(false);
                    }
                        
                } catch (Exception ex) {
                    Logger.getLogger(CommentListCell.class.getName()).log(Level.SEVERE, null, ex);
                }
                ServiceCommentairePost SCp=new ServiceCommentairePost();
                ServicePost Sp=new ServicePost();
                  supprimer.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent event) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("suppression d'un commentaire");
            alert.setHeaderText("Etes-vous sur de vouloir  supprimer ce commentaire?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
              SCp.supprimerCommentaire(item);
                getListView().getItems().remove(getItem());
                }
                }
                });
                 
                  if(SCp.verif_jaime(item))
                  {
                     Image image = new Image("http://localhost//dotcom_2019//web//img//jaime.png",33,23,false,false);
                       like.setGraphic(new ImageView(image));
                      
                  }
                  else { Image image = new Image("http://localhost//dotcom_2019//web//img//jaimepas.png",33,23,false,false);
                       like.setGraphic(new ImageView(image));}
                  
                  like.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent event) {
             if(SCp.verif_jaime(item))
                  {
                     Image image = new Image("http://localhost//dotcom_2019//web//img//jaimepas.png",33,23,false,false);
                       like.setGraphic(new ImageView(image));
                       SCp.AimerCommentaire(item);
                       
                      
                  }
                  else { Image image = new Image("http://localhost//dotcom_2019//web//img//jaime.png",33,23,false,false);
                       like.setGraphic(new ImageView(image));
                        SCp.NePlussAimerCommentaire(item);
              }
             
              
                      nbr_jaime.setText("                                                                                                  "+item.getNbr_jaime()+" J'aimes");
                }
                
                });
                  resolu.setVisible(false);
                 if(SCp.verif_resoudre(item.getPost())) 
                 {
            try {
                    if(item.getUser().getId()==Session.getCurrentSession())
                    {resolu.setVisible(false);}
                    else {resolu.setVisible(true);
                    }
                        
                } catch (Exception ex) {
                    Logger.getLogger(CommentListCell.class.getName()).log(Level.SEVERE, null, ex);
                }    }  
                  
                  
            resolu.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent event) {    
                 Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Topic Résolu");
            alert.setHeaderText("Etes-vous sur que ce commentaire \nrépond exactement à votre besoin?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                SCp.ResoudrePost(item);
                resolu.setVisible(false);
            }
                }
            
            
            } );
                  
                  setGraphic(content);
                
            } else {
                setGraphic(null);
            }
        }
    
}
