/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity.Post;
import Entity.CategoriePost;
import Entity.CommentairePost;
import Entity.Reclamation;
import Entity.Session;
import Service.ServiceCommentairePost;
import Service.ServicePost;
import Service.ServiceReclamation;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import nawara.copyImages;

/**
 * FXML Controller class
 *
 * @author Mohamed Bousselmi
 */
public class PostFrontController implements Initializable {

    @FXML
    private Button btnOverview;
    @FXML
    private Button btnMenus;
    @FXML
    private Button btnCustomers;
    @FXML
    private Button btnPackages;
    @FXML
    private Button btnOrders;
    @FXML
    private Button btnSettings;
    @FXML
    private Button btnSignout;
    @FXML
    private Pane pnlOverview;
    @FXML
    private TabPane tab;
    @FXML
    private Button Annuler;
    @FXML
    private Button Valider;
    @FXML
    private Button btnPhoto;
    
    @FXML
    private Button Retour_topic;
    @FXML
    private Text txtPhoto;
    @FXML
    private JFXTextField titre_post;
    @FXML
    private ChoiceBox Categorie_post;
    @FXML
    private ChoiceBox id_Categorie_post;
    @FXML
    private JFXTextArea Description_post;
    
    @FXML
    private ListView <Post>listView;
     @FXML
    private ListView <CommentairePost>listViewComment;
     @FXML
    private JFXTextArea descript;
    @FXML
    private JFXTextField user;
    @FXML
    private JFXTextField date_envoi;
   
    @FXML
    private JFXTextField titre;
   
    @FXML
    private ImageView image_Post;
    @FXML
    private Label erreur;
    @FXML
    private TextField comment;
   
    @FXML
    private Button aimer;
    @FXML
    private Button nepasaimer;
    @FXML
    private ImageView icon_aimer;
     @FXML
    private ImageView icon_nepasaimer;
    
    
    /**********************My Instance****************/
    private ObservableList<Post> ListPost = FXCollections.observableArrayList();
    Post p = new Post(); 
     ServicePost Ps = new ServicePost();
     CommentairePost cp = new CommentairePost(); 
     ServiceCommentairePost CPs = new ServiceCommentairePost();
     private String absolutePathPhoto;
     
    Stage dialogStage = new Stage();
    Scene scene;
     /***********************************************/
  
   
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Retour_topic.setStyle("-fx-background-color: rgba(255, 255, 255,0);");
        aimer.setStyle("-fx-background-color: rgba(255, 255, 255,0);");
        nepasaimer.setStyle("-fx-background-color: rgba(255, 255, 255,0);");
        
        
        titre.setEditable(false);
        date_envoi.setEditable(false);
        user.setEditable(false);
        descript.setEditable(false);
        
          Afficher();
        List<CategoriePost> list = Ps.getAllCategorie();
             id_Categorie_post.setVisible(false);
             
             erreur.setVisible(false);
               
            
              for (int i=0; i< list.size();i++)
                     {
                        
                         Categorie_post.getItems().add(list.get(i));
                         id_Categorie_post.getItems().add(list.get(i).getId());
                         
                     }
             
               Categorie_post.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
      
    @Override
      public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
        id_Categorie_post.getSelectionModel().select( Categorie_post.getSelectionModel().getSelectedIndex());
          
      }
    });
               
    }    
public void Afficher()
{
    List<Post> lstPost2 = new ArrayList<>();
        
        lstPost2=Ps.getAllFront();
        
        ObservableList<Post> data = FXCollections.observableArrayList(lstPost2);
    
      
     
        
        listView.setCellFactory(new Callback<ListView<Post>, ListCell<Post>>() {
                @Override
                public ListCell<Post> call(ListView<Post> listView) {
                    return new CustomListCell();
                }
             });
        
        listView.setItems(data);
}


public void AfficherCommentaire()
{
    List<CommentairePost> lstComment = new ArrayList<>();
        p=(Post)listView.getSelectionModel().getSelectedItem();
        lstComment=CPs.getAll(p);
        
        ObservableList<CommentairePost> data = FXCollections.observableArrayList(lstComment);
    
      
     
        
        listViewComment.setCellFactory(new Callback<ListView<CommentairePost>, ListCell<CommentairePost>>() {
                @Override
                public ListCell<CommentairePost> call(ListView<CommentairePost> listView) {
                    return new CommentListCell();
                }
             });
        
        listViewComment.setItems(data);
}
        
    @FXML
    private void Annuler_post(ActionEvent event) {
         titre_post.setText("");
        Description_post.setText("");
        txtPhoto.setText("");
        tab.getSelectionModel().select(0);
        id_Categorie_post.getSelectionModel().select(-1);
         Categorie_post.getSelectionModel().select(-1);
         erreur.setVisible(false);
    }

    @FXML
    private void Valider_post(ActionEvent event) {
        
       if(Categorie_post.getSelectionModel().isSelected(-1)|| titre_post.getText().equals("")|| Description_post.getText().equals("")|| txtPhoto.getText().equals(""))
       {  erreur.setVisible(true);}
       else {
              
       int i=(int)id_Categorie_post.getValue();
        Post p = new Post(i,titre_post.getText(),Description_post.getText(), txtPhoto.getText()); 
        ServicePost Ps = new ServicePost();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ajout d'un nouveau Poste");
            alert.setHeaderText("Votre Poste est maintenant sous traitment \nDés qu'il sera valider par notre équipe\nIl sera publié sur nos platformes!!");
            Optional<ButtonType> result = alert.showAndWait();
        copyImages.deplacerVers(txtPhoto, absolutePathPhoto,"C:\\Users\\Mohamed Bousselmi\\Desktop\\Nawara\\src\\images");
         copyImages.deplacerVers(txtPhoto, absolutePathPhoto,"C:\\wamp\\www\\dotcom_2019\\web\\img");
       Ps.ajouterPost(p);
        Afficher();
        titre_post.setText("");
        Description_post.setText("");
        txtPhoto.setText("");
        tab.getSelectionModel().select(0);
        id_Categorie_post.getSelectionModel().select(-1);
         Categorie_post.getSelectionModel().select(-1);
        erreur.setVisible(false);}
    }
    
    @FXML
     private void photoChooser(ActionEvent event)
     {
          FileChooser fileChooser = new FileChooser();
         fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg")
         );
        
            File choix = fileChooser.showOpenDialog(null);
            if (choix != null) {
              
                absolutePathPhoto = choix.getAbsolutePath();
                txtPhoto.setText(choix.getName());
             } else {
                System.out.println("Image introuvable");
            }
        
     }
     
     @FXML
    private void list_clicked(MouseEvent event)
    {
     Post p =(Post)listView.getSelectionModel().getSelectedItem();
           
           titre.setText(p.getTitre());
        date_envoi.setText(p.getDate_envoi().toString());
        user.setText(p.getUser().toString());
        descript.setText(p.getDescription());
         image_Post.setImage(new javafx.scene.image.Image("http://localhost//dotcom_2019//web//img//"+p.getPhoto()));
         AfficherCommentaire();
            tab.getSelectionModel().select(2);
          
            if(Ps.verif_jaime(p))
            {
               
                
                icon_aimer.setVisible(true);
                icon_nepasaimer.setVisible(false);
                aimer.setVisible(true);
                nepasaimer.setVisible(false);
            }
            else
            {
               
                icon_aimer.setVisible(false);
                icon_nepasaimer.setVisible(true);
                aimer.setVisible(false);
                nepasaimer.setVisible(true);
            }
    }
     @FXML
    private void jaime(ActionEvent event)
     {
         Post p =(Post)listView.getSelectionModel().getSelectedItem();  
          Ps.AimerPost(p);
        
                icon_aimer.setVisible(false);
                icon_nepasaimer.setVisible(true);
                aimer.setVisible(false);
                nepasaimer.setVisible(true);
        
     }
          
     @FXML
    private void jenaimepas(ActionEvent event)
     {
         Post p =(Post)listView.getSelectionModel().getSelectedItem();
         Ps.NePlusAimerPost(p);
         
                icon_aimer.setVisible(true);
                icon_nepasaimer.setVisible(false);
                aimer.setVisible(true);
                nepasaimer.setVisible(false);
       
     }        
    @FXML
    private void retourner(ActionEvent event)
     {
           Afficher();
     tab.getSelectionModel().select(0);}
     @FXML
    private void ajouter_topic(ActionEvent event)
     {
     tab.getSelectionModel().select(1);}
    
    @FXML
    private void ajouter_comment(ActionEvent event)
     {
          Post p =(Post)listView.getSelectionModel().getSelectedItem();
          CommentairePost c=new CommentairePost(p,comment.getText());
          CPs.ajouterComment(c);
          AfficherCommentaire();
          comment.setText("");
     }
    @FXML
    private void reclamation_btn(ActionEvent event)
     {
    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Reclamation.fxml")));
        } catch (IOException ex) {
            Logger.getLogger(PostFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
                    dialogStage.setScene(scene);
                    dialogStage.show();
        }
    @FXML
    private void logout_btn(ActionEvent event)
     {
    Node node = (Node)event.getSource();
                    dialogStage = (Stage) node.getScene().getWindow();
                    dialogStage.close();
        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("/gui/Login.fxml")));
        } catch (IOException ex) {
            Logger.getLogger(PostFrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Session.close();
         } catch (Exception ex) {
             Logger.getLogger(ReclamationController.class.getName()).log(Level.SEVERE, null, ex);
         }
                    dialogStage.setScene(scene);
                    dialogStage.show();
        }
    
    

}