/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity.CommentairePost;
import Entity.Post;
import Entity.Reclamation;
import Entity.Session;
import InterfaceService.IServiceCommentairePost;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nawara.MyConnection;

/**
 *
 * @author Mohamed Bousselmi
 */
public class ServiceCommentairePost implements IServiceCommentairePost{
     Connection cn = MyConnection.getInstance().getConnection();
    Statement st;
    PreparedStatement pst;
      @Override
    public List<CommentairePost> getAll(Post p) {
      List<CommentairePost> ListC = new ArrayList<>();
          String    req = "select * from reponse  where postid='"+p.getId()+"' order by id desc ";
          
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
                                 CommentairePost Cp=new CommentairePost(result.getInt(1),new ServicePost().findByIdPost(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getDate(4),result.getInt(5),result.getInt(6),result.getString(7));
                                 ListC.add(Cp);
                                 
            }
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return ListC;
        
    }
    
    @Override
    public void ajouterComment(CommentairePost c) {
    String req=null;
    int i =getnbrComment(c.getPost());
    i++;
    String req1="UPDATE  post SET nbr_reponse='"+i+"'  where id ='"+c.getPost().getId()+"';";
          try {
              req = "INSERT INTO reponse(postid,user_id,description) VALUES ('"+c.getPost().getId()+"','"+Session.getCurrentSession()+"','"+c.getDescription()+"');";
          } catch (Exception ex) {
              Logger.getLogger(ServiceReclamation.class.getName()).log(Level.SEVERE, null, ex);
          }
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
           st.executeUpdate(req1);
          
       }catch(SQLException e){
           System.out.println("error");
       }

    }
    
     @Override
    public void supprimerCommentaire(CommentairePost Cp) {
       String req = "DELETE FROM reponse where id ='"+Cp.getId()+"';";
       String req1 ="DELETE FROM like_comment where  id_comment='"+Cp.getId()+"';";
       
        int i =getnbrComment(Cp.getPost());
    i--;
    String req2="UPDATE  post SET nbr_reponse='"+i+"'  where id ='"+Cp.getPost().getId()+"';";
       try{
           st=cn.createStatement();
           st.executeUpdate(req2);
           st.executeUpdate(req1);
           st.executeUpdate(req);
          
       }catch(SQLException e){
           System.out.println("error");
       }
       
       
    
}
    
    
    
     @Override
     public boolean verif_jaime(CommentairePost Cp)
    {
      
        String req=null;
          try {
              req = "select * from like_comment where id_user='"+Session.getCurrentSession()+"' and id_comment='"+Cp.getId()+"';";
          } catch (Exception ex) {
              Logger.getLogger(ServicePost.class.getName()).log(Level.SEVERE, null, ex);
          }
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            if(!result.isBeforeFirst())
            {
                return true;
            }
            else return false;
                
            
        }catch (SQLException e)
        { e.printStackTrace();}
        
    
        
        return false;
    }
    
    @Override
      public void AimerCommentaire(CommentairePost Cp) {
       Cp.setNbr_jaime(Cp.getNbr_jaime()+1);
       String req = "UPDATE  reponse SET nbr_jaime='"+Cp.getNbr_jaime()+"'  where id ='"+Cp.getId()+"';";
       String req1=null;
       
          try {
              req1 =  "INSERT INTO like_comment(id_user,id_comment) VALUES ('"+Session.getCurrentSession()+"','"+Cp.getId()+"');";
          } catch (Exception ex) {
              Logger.getLogger(ServiceReclamation.class.getName()).log(Level.SEVERE, null, ex);
          }
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
           st.executeUpdate(req1);
          
       }catch(SQLException e){
           System.out.println("error");
       }
    }
    
    @Override
     public void NePlussAimerCommentaire(CommentairePost Cp) {
       String req1=null;
       Cp.setNbr_jaime(Cp.getNbr_jaime()-1);
       String req = "UPDATE  reponse SET nbr_jaime='"+Cp.getNbr_jaime()+"'  where id ='"+Cp.getId()+"';";
       try {
              req1 ="DELETE FROM like_comment where id_user='"+Session.getCurrentSession()+"' and id_comment='"+Cp.getId()+"';";
          } catch (Exception ex) {
              Logger.getLogger(ServiceReclamation.class.getName()).log(Level.SEVERE, null, ex);
          }
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
          st.executeUpdate(req1);
       }catch(SQLException e){
           System.out.println("error");
       }
    }
    
      public int getnbrComment(Post p) {
      List<CommentairePost> ListC = new ArrayList<>();
          String    req = "select * from reponse  where postid='"+p.getId()+"' order by id desc ";
          
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
                                 CommentairePost Cp=new CommentairePost(result.getInt(1),new ServicePost().findByIdPost(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getDate(4),result.getInt(5),result.getInt(6),result.getString(7));
                                 ListC.add(Cp);
                                 
            }
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return ListC.size();
        
    }
      
      
        @Override
    public void ResoudrePost(CommentairePost Cp) {
       
       String req = "UPDATE  post SET status='1'  where id ='"+Cp.getPost().getId()+"';";
       String req1 = "UPDATE  reponse SET nbr_alerte='1'  where id ='"+Cp.getId()+"';";
       try{
           st=cn.createStatement();
         st.executeUpdate(req1);
           st.executeUpdate(req);
        
       }catch(SQLException e){
           System.out.println("error");
       }
    }
    
    
    @Override
    public boolean verif_resoudre(Post p)
    {
      
        String req=null;
          try {
              req = "select * from post where status='1' and id='"+p.getId()+"';";
          } catch (Exception ex) {
              Logger.getLogger(ServicePost.class.getName()).log(Level.SEVERE, null, ex);
          }
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            if(!result.isBeforeFirst())
            {
                return true;
            }
            else return false;
                
            
        }catch (SQLException e)
        { e.printStackTrace();}
        
    
        
        return false;
    }
    
    
}
