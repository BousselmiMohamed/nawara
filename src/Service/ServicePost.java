/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Entity.CategoriePost;
import Entity.CommentairePost;
import Entity.Post;
import Entity.Reclamation;
import Entity.Session;
import Entity.Utilisateur;
import InterfaceService.IServicePost;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import nawara.MyConnection;

/**
 *
 * @author Mohamed Bousselmi
 */
public class ServicePost implements IServicePost{
    
     Connection cn = MyConnection.getInstance().getConnection();
    Statement st;
    ResultSet rs;
    PreparedStatement pst;
    
    @Override
    public void ajouterPost(Post p) {
    String req=null;
       
          try {
              req = "INSERT INTO post(catid,user_id,description,photo,titre) VALUES ('"+p.getCatId()+"','"+Session.getCurrentSession()+"','"+p.getDescription()+"','"+p.getPhoto()+"','"+p.getTitre()+"');";
          } catch (Exception ex) {
              Logger.getLogger(ServiceReclamation.class.getName()).log(Level.SEVERE, null, ex);
          }
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
         
       }catch(SQLException e){
           System.out.println("error");
       }
    
    }
   
    @Override
    public List<Post> getAll() {
      List<Post> Listp = new ArrayList<>();
        String req=null;
          try {
              req = "select * from post order by id desc";
          } catch (Exception ex) {
              Logger.getLogger(ServicePost.class.getName()).log(Level.SEVERE, null, ex);
          }
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
                if (result.getInt(8)==0)
                {
                    Post p = new Post(result.getInt(1),new ServicePost().findByIdCategorie(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getString(12),result.getString(4),result.getString(5),result.getDate(6),result.getDate(7),"En Attente",result.getInt(9),result.getInt(10),result.getInt(11));
                    
                    Listp.add(p);
                }
                else if (result.getInt(8)==1)
                {
                Post p = new Post(result.getInt(1),new ServicePost().findByIdCategorie(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getString(12),result.getString(4),result.getString(5),result.getDate(6),result.getDate(7),"Publiée",result.getInt(9),result.getInt(10),result.getInt(11));
                Listp.add(p);
                }
                 else if (result.getInt(8)==2)
                {
                Post p = new Post(result.getInt(1),new ServicePost().findByIdCategorie(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getString(12),result.getString(4),result.getString(5),result.getDate(6),result.getDate(7),"Refusé",result.getInt(9),result.getInt(10),result.getInt(11));
                Listp.add(p);
                }
            }
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return Listp;
        
    }
     @Override
    public List<Post> getAllFront() {
      List<Post> Listp = new ArrayList<>();
        String req=null;
          try {
              req = "select * from post order by id desc";
          } catch (Exception ex) {
              Logger.getLogger(ServicePost.class.getName()).log(Level.SEVERE, null, ex);
          }
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
               if (result.getInt(8)==1)
                {
                Post p = new Post(result.getInt(1),new ServicePost().findByIdCategorie(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getString(12),result.getString(4),result.getString(5),result.getDate(6),result.getDate(7),"Publiée",result.getInt(9),result.getInt(10),result.getInt(11));
                Listp.add(p);
                }
                
            }
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return Listp;
        
    }
      @Override
    public List<CategoriePost> getAllCategorie() {
      List<CategoriePost> Listp = new ArrayList<>();
        String req=null;
          try {
              req = "select * from categoriepost";
          } catch (Exception ex) {
              Logger.getLogger(ServicePost.class.getName()).log(Level.SEVERE, null, ex);
          }
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
                
                    CategoriePost p = new CategoriePost(result.getInt(1),result.getString(2));
                    
                    Listp.add(p);
               
                
                }
            
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return Listp;
        
    }
    
    
    
     @Override
    public List<Post> rechercherPost(String x) {
      List<Post> Listp = new ArrayList<>();
        String req=null;
          try {
              req = "select * from post where titre like '"+x+"%' or date_envoi like '"+x+"%' order by id desc";
          } catch (Exception ex) {
              Logger.getLogger(ServicePost.class.getName()).log(Level.SEVERE, null, ex);
          }
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            
            while(result.next()){
                if (result.getInt(8)==0)
                {
                    Post p = new Post(result.getInt(1),new ServicePost().findByIdCategorie(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getString(12),result.getString(4),result.getString(5),result.getDate(6),result.getDate(7),"En Attente",result.getInt(9),result.getInt(10),result.getInt(11));
                    
                    Listp.add(p);
                }
                else if (result.getInt(8)==1)
                {
                Post p = new Post(result.getInt(1),new ServicePost().findByIdCategorie(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getString(12),result.getString(4),result.getString(5),result.getDate(6),result.getDate(7),"Publiée",result.getInt(9),result.getInt(10),result.getInt(11));
                Listp.add(p);
                }
                 else if (result.getInt(8)==2)
                {
                Post p = new Post(result.getInt(1),new ServicePost().findByIdCategorie(result.getInt(2)),new ServiceUser().findById(result.getInt(3)),result.getString(12),result.getString(4),result.getString(5),result.getDate(6),result.getDate(7),"Refusé",result.getInt(9),result.getInt(10),result.getInt(11));
                Listp.add(p);
                }
            }
        }catch (SQLException e)
        { e.printStackTrace();}
        
        return Listp;
        
    }
    
     @Override
    public void supprimerPost(Post p) {
       String req = "DELETE FROM post where id ='"+p.getId()+"';";
       String req1 ="DELETE FROM likes where   postid='"+p.getId()+"';";
       
       try{
           st=cn.createStatement();
           ServiceCommentairePost SCp =new ServiceCommentairePost();
           List<CommentairePost> lc =SCp.getAll(p);
             for(CommentairePost c:lc) SCp.supprimerCommentaire(c);
             
           st.executeUpdate(req1);
           st.executeUpdate(req);
          
       }catch(SQLException e){
           System.out.println("error");
       }
    }
    
     @Override
    public void AccepterPost(Post p) {
       
       String req = "UPDATE  post SET etat='1'  where id ='"+p.getId()+"';";
       
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
           
          
       }catch(SQLException e){
           System.out.println("error");
       }
    }
      @Override
    public void RefuserPost(Post p) {
       
       String req = "UPDATE  post SET etat='2'  where id ='"+p.getId()+"';";
       
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
          
       }catch(SQLException e){
           System.out.println("error");
       }
    }
    @Override
    public boolean verif_jaime(Post p)
    {
      
        String req=null;
          try {
              req = "select * from likes where user_id='"+Session.getCurrentSession()+"' and postid='"+p.getId()+"';";
          } catch (Exception ex) {
              Logger.getLogger(ServicePost.class.getName()).log(Level.SEVERE, null, ex);
          }
        PreparedStatement ps ;
        try{
            ps=cn.prepareStatement(req);
            ResultSet result = ps.executeQuery();
            if(!result.isBeforeFirst())
            {
                return true;
            }
            else return false;
                
            
        }catch (SQLException e)
        { e.printStackTrace();}
        
    
        
        return false;
    }
    
    @Override
    public void AimerPost(Post p) {
       p.setNbr_jaime(p.getNbr_jaime()+1);
       String req = "UPDATE  post SET nbr_jaime='"+p.getNbr_jaime()+"'  where id ='"+p.getId()+"';";
       String req1=null;
       
          try {
              req1 =  "INSERT INTO likes(postid,user_id) VALUES ('"+p.getId()+"','"+Session.getCurrentSession()+"');";
          } catch (Exception ex) {
              Logger.getLogger(ServiceReclamation.class.getName()).log(Level.SEVERE, null, ex);
          }
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
           st.executeUpdate(req1);
          
       }catch(SQLException e){
           System.out.println("error");
       }
    }
    
    @Override
    public void NePlusAimerPost(Post p) {
       String req1=null;
       p.setNbr_jaime(p.getNbr_jaime()-1);
       String req = "UPDATE  post SET nbr_jaime='"+p.getNbr_jaime()+"'  where id ='"+p.getId()+"';";
       try {
              req1 ="DELETE FROM likes where user_id='"+Session.getCurrentSession()+"' and postid='"+p.getId()+"';";
          } catch (Exception ex) {
              Logger.getLogger(ServiceReclamation.class.getName()).log(Level.SEVERE, null, ex);
          }
       try{
           st=cn.createStatement();
           st.executeUpdate(req);
          st.executeUpdate(req1);
       }catch(SQLException e){
           System.out.println("error");
       }
    }
    
   
    
    @Override
     public CategoriePost findByIdCategorie(int id)throws SQLException{ 
        {
            CategoriePost u = new CategoriePost();
            String requete="select * from categoriepost where id ='"+id+"';";
           pst=cn.prepareStatement(requete);
           rs=pst.executeQuery(requete); 
            while(rs.next())
            {
                u=new CategoriePost(rs.getInt(1), rs.getString(2));
            }
            
           return u;   
        }
     
     }
     
     
     @Override
     public Post findByIdPost(int id)throws SQLException{ 
        {
            Post p = new Post();
            String requete="select * from post where id ='"+id+"';";
           pst=cn.prepareStatement(requete);
           rs=pst.executeQuery(requete); 
            while(rs.next())
            {
                p=new Post(rs.getInt(1),new ServicePost().findByIdCategorie(rs.getInt(2)),new ServiceUser().findById(rs.getInt(3)),rs.getString(12),rs.getString(4),rs.getString(5),rs.getDate(6),rs.getDate(7),"Publiée",rs.getInt(9),rs.getInt(10),rs.getInt(11));
                    
            }
            
           return p;   
        }
     
     } 
     

      public ObservableList<String> Stat(){
        int val=0 ;
        String req= "SELECT COUNT(*) as 'num', c.label FROM post p join categoriepost c on p.catid= c.id GROUP BY catid";
            ObservableList<String> Statistique=FXCollections.observableArrayList();
        try {
            pst=cn.prepareStatement(req);
            ResultSet resultSet=pst.executeQuery();

              while(resultSet.next()){
         
            String number = (resultSet.getString(1));
            Statistique.add(number);
            String lib = (resultSet.getString(2));
            Statistique.add(lib);
            
              
              }
        } catch (SQLException ex) {
            ex.printStackTrace();
          
        }
        return Statistique;
      }
    
    
     
     public  int[]   Stat3(){
        int[] monthCounter = new int[12];
       int val=0; 
        String req= "SELECT month(date_envoi) 'month',count(*) from post where (etat='1') group by month(date_envoi)";

        try {
            pst=cn.prepareStatement(req);
            ResultSet resultSet=pst.executeQuery();
     while(resultSet.next()){
         for(int i = 0; i < 12; i++)
{
        if(((Number) resultSet.getObject(1)).intValue()==i)
        {monthCounter[i] =  ((Number) resultSet.getObject(2)).intValue();
             
        }
              }}
        } catch (SQLException ex) {
            ex.printStackTrace();
          
        }
        return monthCounter;
      }
}
